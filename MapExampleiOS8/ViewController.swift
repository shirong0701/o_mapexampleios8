//
//  ViewController.swift
//  MapExampleiOS8
//


import UIKit
import MapKit

enum MapOption: Int {
    case kMapOptionNone = 0
    case kMapUserLocation = 1
    case kMapOptionAnnotation = 2
    case kMapOptionPolyline = 3
    case kMapOptionPolygon = 4
    case kMapOptionCircle = 5
    case kMapOptionDirections = 6
    case kMapOptionCamera = 7
    case kMapOptionTile = 8
    case kMapOptionCluster = 9
    case kMapOptionSearch = 10
    case kMapOptionSnapShot = 11
}

class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var mapView : MKMapView!
    @IBOutlet weak var tableView : UITableView!

    var locationManager = CLLocationManager()
    var mapOptions:     NSArray!
    var cameraItems:    NSMutableArray!
    var mapOption:      MapOption!
    
    // MARK: - Memory management methods
    
    deinit {
        tableView.dataSource = nil
        tableView.delegate = nil
    }
    
    // MARK: - Action methods

    @IBAction func openMenuButtonTapped(sender: UIBarButtonItem) {
        self.closeMenuView()
    }
    
    // MARK: - Custom methods
    
    func chooseOption(itemIndex: Int) {
        
        self.mapOption = MapOption(rawValue: itemIndex)
        
        switch (itemIndex) {
            case 0:
                println()
            
            case 1:
                MapHelper().showUserLocation(self.mapView)
            
            case 2:
                MapHelper().addCustomAnnotations(self.mapView)

            case 3:
                MapHelper().createPolyline(self.mapView)

            case 4:
                MapHelper().createPolygon(self.mapView)
            
            case 5:
                MapHelper().createCircle(self.mapView)

            case 6:
                MapHelper().setDirectionLocation(self.mapView)
            
            case 7:
                self.setCameraLocations()

            default:
                println("nothing happens")
        }
        
        self.mapView(self.mapView, regionDidChangeAnimated: true)

    }
    
    func closeMenuView() {
        UIView.animateWithDuration(0.35, animations: { () -> Void in
            
            var tblRect = self.tableView.frame
            if (self.tableView.hidden) {
                self.tableView.hidden = false
            } else {
                self.tableView.hidden = true
            }
            
//            self.tableView.frame = tblRect
//            self.view.layoutIfNeeded()
        })
    }
    
    func addBlurEffect() {
        // Add blur view
        var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light)) as UIVisualEffectView
        visualEffectView.frame = tableView.bounds
        visualEffectView.autoresizingMask = .FlexibleHeight | .FlexibleWidth
        visualEffectView.setTranslatesAutoresizingMaskIntoConstraints(false)
        tableView.addSubview(visualEffectView)
        tableView.sendSubviewToBack(visualEffectView)
    }
    
//    func removeMapOverlay() {
//        println(self.mapView.overlays)
//        let removeOverlays : [AnyObject]! = self.mapView.overlays
//        self.mapView.removeOverlays(removeOverlays)
//        
//        var overlays = NSMutableArray(array: self.mapView.overlays) as NSMutableArray!
//        if (overlays.containsObject(MKUserLocation.self())) {
//            overlays.removeObject(MKUserLocation.self())
//        }
//        
//        var annotations = NSArray(array: overlays)
//        self.mapView.removeAnnotations(annotations)
//    }
    
    func setCameraLocations() {
        var centerPoints = CLLocationCoordinate2DMake(37.79520324238053, -122.40283370018005)
        var endPoints = CLLocationCoordinate2DMake(41.890289963005124, 12.492302656173706)
        
        var location1 = CLLocation(latitude: centerPoints.latitude, longitude: centerPoints.longitude)
        var location2 = CLLocation(latitude: endPoints.latitude, longitude: endPoints.longitude)
        var distance = location2.distanceFromLocation(location1)
        
        var camera1 = MKMapCamera(lookingAtCenterCoordinate: centerPoints, fromEyeCoordinate: centerPoints, eyeAltitude: 300)
        var camera2 = MKMapCamera(lookingAtCenterCoordinate: centerPoints, fromEyeCoordinate: centerPoints, eyeAltitude: distance)
        var camera3 = MKMapCamera(lookingAtCenterCoordinate: endPoints, fromEyeCoordinate: endPoints, eyeAltitude: distance)
        var camera4 = MKMapCamera(lookingAtCenterCoordinate: endPoints, fromEyeCoordinate: endPoints, eyeAltitude: 800)
        
        self.mapView.setCamera(camera1, animated: true)
        self.cameraItems = NSMutableArray(array: [camera2, camera3, camera4])
    }
    
    func dropShape() {
        
    }
    
    func dropShape(points: CGPoint) {
        
    }
    
    func goToNextCamera() {
        if (self.cameraItems.count == 0) {
            return
        }
        
        var nextCamera = self.cameraItems.firstObject as MKMapCamera
        self.cameraItems.removeObjectAtIndex(0)
        
        UIView.animateWithDuration(1.5, delay: 0.5, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
            self.mapView.setCamera(nextCamera, animated: true)
        }, completion: nil)
    }
    
    func addBounceAnimationToView(view: UIView) {
        var bounceAnimation = CAKeyframeAnimation(keyPath: "transform.scale") as CAKeyframeAnimation
        bounceAnimation.values = [ 0.05, 1.1, 0.9, 1]

        var timingFunctions = NSMutableArray(capacity: bounceAnimation.values.count)
        
        for var i = 0; i < bounceAnimation.values.count; i++ {
            timingFunctions.addObject(CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut))
        }
        bounceAnimation.timingFunctions = timingFunctions
        bounceAnimation.removedOnCompletion = false
        
        view.layer.addAnimation(bounceAnimation, forKey: "bounce")
    }
    
    // MARK: UITableView datasource methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mapOptions.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("CELL") as UITableViewCell!
        
        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "CELL")
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.textLabel.textColor = UIColor(red: 0.78039, green: 0.27450, blue: 0.41568, alpha: 1.0)
        cell.textLabel.text = mapOptions.objectAtIndex(indexPath.row) as? String
        
        cell?.backgroundColor = UIColor.clearColor()

        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        for (index, value) in enumerate(mapOptions) {
            var previousCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0))
            previousCell?.backgroundColor = UIColor.clearColor()
            previousCell?.textLabel.textColor = UIColor(red: 0.78039, green: 0.27450, blue: 0.41568, alpha: 1.0)
        }
        
        var cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.backgroundColor = UIColor(red: 0.78039, green: 0.27450, blue: 0.41568, alpha: 1.0)
        cell?.textLabel.textColor = UIColor.whiteColor()
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.closeMenuView()
        
        self.chooseOption(indexPath.row)
    }
    
    // MARK: - CLLocationManager delegate methods
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case .Authorized, .AuthorizedWhenInUse:
            manager.startUpdatingLocation()
            self.mapView.showsUserLocation = true
        default: break
        }
    }
    
    // MARK: - MapView delegate methods
    
    func mapView(mapView: MKMapView!, didAddAnnotationViews views: [AnyObject]!) {
        for view: UIView! in views as [UIView] {
            self.addBounceAnimationToView(view)
        }
    }
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        if (annotation is MKUserLocation) {
            return nil
        }

        
        if (annotation.isKindOfClass(CustomAnnotation)) {
            var customAnnotation = annotation as? CustomAnnotation
            mapView.setTranslatesAutoresizingMaskIntoConstraints(false)
            var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier("CustomAnnotation") as MKAnnotationView!
            
            if (annotationView == nil) {
                annotationView = customAnnotation?.annotationView()
            } else {
                annotationView.annotation = annotation;
            }
            
            self.addBounceAnimationToView(annotationView)
            return annotationView
        } else {
            return nil
        }
    }
    
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        var color = UIColor(red: 0.78039, green: 0.27450, blue: 0.41568, alpha: 1.0)

        if (overlay is MKTileOverlay) {
            return MKTileOverlayRenderer(tileOverlay: overlay as MKTileOverlay!)
        }
        
        if (overlay is MKPolyline) {
            var renderer = MKPolylineRenderer(overlay: overlay)
            renderer.lineWidth = 5.0
            renderer.strokeColor = color
            return renderer
            
        } else if (overlay is MKCircle) {
            var renderer = MKCircleRenderer(circle: overlay as MKCircle!)
            renderer.strokeColor = color
            renderer.fillColor = color.colorWithAlphaComponent(0.4)
            return renderer
        } else {
            var renderer = MKPolylineRenderer(polyline: overlay as MKPolyline!)
            renderer.strokeColor = color
            renderer.lineWidth = 6.0
            return renderer
        }
    }
    
    func mapView(mapView: MKMapView!, didSelectAnnotationView view: MKAnnotationView!) {
        var customAnnotation = view.annotation as CustomAnnotation!
    }
    
    func mapView(mapView: MKMapView!, regionDidChangeAnimated animated: Bool) {
        if (self.mapOption == MapOption.kMapOptionCamera) {
            if (animated) {
                self.goToNextCamera()
            }
        }
        
        if (self.mapOption == MapOption.kMapOptionCluster) {
            NSOperationQueue().addOperationWithBlock({
                var scale = CGFloat(0.0)
                scale = self.mapView.bounds.size.width / CGFloat(self.mapView.visibleMapRect.size.width)
                
            })
        }
    }
    
    func mapView(mapView: MKMapView!, didUpdateUserLocation userLocation: MKUserLocation!) {
        MapHelper().showUserLocation(self.mapView)
    }
    
    // MARK: - View lifeCycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mapOption = MapOption.kMapOptionNone
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
        locationManager.startUpdatingLocation()
        
        self.mapView.delegate = self
        self.mapView.showsUserLocation = true
        self.mapView.pitchEnabled = true
        self.mapView.showsBuildings = true
        MapHelper().showUserLocation(self.mapView)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.clearColor()
        tableView.scrollEnabled = false
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        self.addBlurEffect()
        
        mapOptions = ["None", "Show location","Annotations", "Polyline", "Polygon", "Circle", "Directions", "Camera", "Tile", "Cluster", "Search", "SnapShot"];
        
        var newYorkLocation = CLLocationCoordinate2DMake(40.730872, -74.003066)
        // Drop a pin
        var dropPin = MKPointAnnotation()
        dropPin.coordinate = newYorkLocation
        dropPin.title = "New York City"
        mapView.addAnnotation(dropPin)

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName : UIColor(red: 0.78039, green: 0.27450, blue: 0.41568, alpha: 1.0)]
    }
}

