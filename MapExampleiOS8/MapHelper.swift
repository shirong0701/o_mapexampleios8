//
//  MapHelper.swift
//  MapExampleiOS8
//


import UIKit
import MapKit

class MapHelper: NSObject {
   
    func showUserLocation(mapView: MKMapView) {
        UIView.animateWithDuration(1.5, animations: { () -> Void in
            let span = MKCoordinateSpanMake(1, 1)
            let region1 = MKCoordinateRegion(center: mapView.userLocation.coordinate, span: span)
            mapView.setRegion(region1, animated: true)
        })
    }
 
    func addCustomAnnotations(mapView: MKMapView) {
        // Remove annotations
        //        self.removeMapOverlay()
        var url = NSURL(string: "https://google.com")
        
        // New york
        var newYorkAnnotation = CustomAnnotation(coordinate: CLLocationCoordinate2DMake(40.730872, -74.003066), title: "New York", subtitle: "City", detailURL: url!)
        mapView.addAnnotation(newYorkAnnotation)
        
        // Los Angeles
        var losAngelesAnnotation = CustomAnnotation(coordinate: CLLocationCoordinate2DMake(34.049519, -118.221532), title: "Los Angeles", subtitle: "City", detailURL: url!)
        mapView.addAnnotation(losAngelesAnnotation)
        
        
        // Sydney
        var sydneyAnnotation = CustomAnnotation(coordinate: CLLocationCoordinate2DMake(-33.868220, 151.200524), title: "Sydney", subtitle: "City", detailURL: url!)
        mapView.addAnnotation(sydneyAnnotation)
        
        // New Delhi
        var newDelhiAnnotation = CustomAnnotation(coordinate: CLLocationCoordinate2DMake(28.640922, 77.229803), title: "New Delhi", subtitle: "City", detailURL: url!)
        mapView.addAnnotation(newDelhiAnnotation)
        
        // Rio de janero
        var rioDeJaneroAnnotation = CustomAnnotation(coordinate: CLLocationCoordinate2DMake(-22.910315, -43.211780), title: "Rio de janero", subtitle: "City", detailURL: url!)
        mapView.addAnnotation(rioDeJaneroAnnotation)
        
        // Tokiyo
        var tokiyoAnnotation = CustomAnnotation(coordinate: CLLocationCoordinate2DMake(35.630660, 139.597435), title: "Tokiyo", subtitle: "City", detailURL: url!)
        mapView.addAnnotation(tokiyoAnnotation)
        
        // London
        var londonAnnotation = CustomAnnotation(coordinate: CLLocationCoordinate2DMake(51.473277, -0.111903), title: "London", subtitle: "City", detailURL: url!)
        mapView.addAnnotation(londonAnnotation)
        
        // Beijing
        var beijingAnnotation = CustomAnnotation(coordinate: CLLocationCoordinate2DMake(39.909922, 116.407759), title: "Beijing", subtitle: "City", detailURL: url!)
        mapView.addAnnotation(beijingAnnotation)
        
        // Johannesburg
        var johannesburgAnnotation = CustomAnnotation(coordinate: CLLocationCoordinate2DMake(-26.195462, 28.052942), title: "Johannesburg", subtitle: "City", detailURL: url!)
        mapView.addAnnotation(johannesburgAnnotation)
        
        // Map zoom out
        
        UIView.animateWithDuration(1.5, animations: { () -> Void in
            let span = MKCoordinateSpanMake(100, 100)
            let region = MKCoordinateRegion(center: mapView.userLocation.coordinate, span: span)
            mapView.region = region
        })
    }
    
    func createPolyline(mapView: MKMapView) {
        var sanFrancisco = CLLocationCoordinate2DMake(37.774929, -122.419416)
        var newYork = CLLocationCoordinate2DMake(40.714353, -74.005973)
        var points: [CLLocationCoordinate2D]
        points = [sanFrancisco, newYork]
        
        var geodesic = MKGeodesicPolyline(coordinates: &points[0], count: 2)
        mapView.addOverlay(geodesic)
        
        UIView.animateWithDuration(1.5, animations: { () -> Void in
            let span = MKCoordinateSpanMake(20, 20)
            let region1 = MKCoordinateRegion(center: sanFrancisco, span: span)
            mapView.setRegion(region1, animated: true)
        })
    }
    
    func createPolygon(mapView: MKMapView) {
        
        var point1 = MKMapPointMake(41.000512, -109.050116)
        var point2 = MKMapPointMake(41.002371, -102.052066)
        var point3 = MKMapPointMake(36.993076, -102.041981)
        var point4 = MKMapPointMake(36.99892, -109.045267)
        
        var pts = [ point1, point2, point3, point4]
        
        let polygon = MKPolygon(points: &pts, count: 4)
        polygon.title = "Colorado"
        mapView.addOverlay(polygon)
        
        UIView.animateWithDuration(1.5, animations: { () -> Void in
            let span = MKCoordinateSpanMake(20, 20)
            let region1 = MKCoordinateRegion(center: CLLocationCoordinate2DMake(41.000512, -109.050116), span: span)
            mapView.setRegion(region1, animated: true)
        })
    }
    
    func createCircle(mapView: MKMapView) {
        var fenceDistance = 100000 as CLLocationDistance!
        var circleMidPoint = CLLocationCoordinate2DMake(51.473277, -0.111903)
        
        var circle = MKCircle(centerCoordinate: circleMidPoint, radius: fenceDistance)
        mapView.addOverlay(circle)
        
        UIView.animateWithDuration(1.5, animations: { () -> Void in
            let span = MKCoordinateSpanMake(20, 20)
            let region1 = MKCoordinateRegion(center: CLLocationCoordinate2DMake(51.473277, -0.111903), span: span)
            mapView.setRegion(region1, animated: true)
        })
    }
    
    func setDirectionLocation(mapView: MKMapView) {
        var location = CLLocationCoordinate2DMake(37.79520324238053, -122.40283370018005)
        let region = MKCoordinateRegion(center: location, span: MKCoordinateSpanMake(0.02, 0.02))
        var adjustRegion = mapView.regionThatFits(region)
        mapView.region = adjustRegion
        
        var toCoordinate = CLLocationCoordinate2DMake(37.802932, -122.401612)
        var fromPlaceMark = MKPlacemark(coordinate: location, addressDictionary: nil)
        var toPlaceMark = MKPlacemark(coordinate: toCoordinate, addressDictionary: nil)
        
        var fromItem = MKMapItem(placemark: fromPlaceMark)
        var toItem = MKMapItem(placemark: toPlaceMark)
        
        var request = MKDirectionsRequest()
        request.setSource(fromItem)
        request.setDestination(toItem)
        request.requestsAlternateRoutes = true
        
        var directions = MKDirections(request: request)
        directions .calculateDirectionsWithCompletionHandler { (var response: MKDirectionsResponse!, var error: NSError!) -> Void in
            if (error != nil) {
                var route = response.routes[0] as? MKRoute
                mapView.addOverlay(route?.polyline)
                
                // Drop a pin
                var startPin = MKPointAnnotation()
                startPin.coordinate = location
                startPin.title = "Start"
                mapView.addAnnotation(startPin)
                
                var endPin = MKPointAnnotation()
                endPin.coordinate = toCoordinate
                endPin.title = "End"
                mapView.addAnnotation(endPin)
            } else {
                println("Directions not found")
            }
        }
    }
}
